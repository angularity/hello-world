import { Component } from '@angular/core';
import { Hero } from './hero';

const HEROES: Hero[] = [
  { id: 1, name: 'Monokai Man' },
  { id: 2, name: 'The Emacser' },
  { id: 3, name: 'VimVillain' },
  { id: 4, name: 'Y-Combinatorus' },
  { id: 5, name: 'Java Man' },
  { id: 6, name: 'Dr. Racket' },
  { id: 7, name: 'Captain Brainfuck' },
  { id: 8, name: 'Regexer' },
  { id: 9, name: 'Mr. EMCAscript' },
  { id: 10, name: 'Jr. JQuery' },
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'Tour of Heroes';
    selectedHero: Hero;
    heroes = HEROES;

    onSelect(hero: Hero): void {
      this.selectedHero = hero;
    }
}
